  name := "actor-lib-poc"
  organization := "com.sample.poc"
  version := "1.1-SNAPSHOT"
  scalaVersion := "2.12.3"



lazy val root = (project in file("."))

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.4",
  "com.typesafe" % "config" % "1.3.1",
  "com.typesafe.akka" %% "akka-slf4j" % "2.5.4",
  "ch.qos.logback" % "logback-classic" % "1.2.3"
  
)

