package com.sample.poc

import akka.actor.{ActorRef, ActorSystem, Props}

import scala.concurrent.ExecutionContext


trait Printable {

  implicit def executionContext: ExecutionContext

  lazy val actorSystem = ActorSystem(name = "printer", defaultExecutionContext = Some(executionContext))

  /**
    * Send a label to an logging actor 
    * @param label label to print
    */
  class Printer(label: String = "") {
    
    implicit val printActor: ActorRef = actorSystem.actorOf(Props(new PrintActor), name = "printactor")

    def execute(): Unit = {
      printActor ! label
    }
  }

  object Printer {
    def apply(label: String)(): Unit = {
      new Printer(label).execute()
    }
  }
  
}
