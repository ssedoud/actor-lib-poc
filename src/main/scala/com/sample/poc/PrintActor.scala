package com.sample.poc

import akka.actor.{Actor, ActorLogging}

class PrintActor extends Actor with ActorLogging {

  override def receive = {
    case message => log.info(s"print message : $message")
  }
}
